(ns acquisition.tcp
  "TCP server/client components"
  (:require [com.stuartsierra.component :refer (Lifecycle)]
            [schema.core :as s :refer (defschema)]
            [plumbing.core :refer (defnk)]
            [aleph.tcp :as tcp]
            [taoensso.timbre :as t]
            [manifold.stream :refer (close! stream? sink? try-put!)]
            [manifold.deferred :as d])
  (:import [io.netty.handler.ssl SslContext]
           [java.net SocketAddress]))

(def max-port
  "Highest possible port number"
  (dec (bit-shift-left 1 16)))

(defschema Port
  "A valid port number"
  (s/pred #(< 1 % max-port) 'port?))

(defrecord TCPClient [opts stream]
  Lifecycle
  (start [this]
    (if stream this
      (assoc this :client @(tcp/client opts))))
  (stop [this]
    (if-not stream this
      (do (close! stream)
          (assoc this :stream nil)))))

(defnk tcp-client :- (s/protocol Lifecycle)
  "The duplex stream is available at :stream in the started component"
  [host :- String, port :- Port]
  (->TCPClient {:host host :port port} nil))

(defrecord TCPServer [on-close handler opts server]
  Lifecycle
  (start [this]
    (if server this
      (assoc this :server (tcp/start-server handler opts))))
  (stop [this]
    (if-not server this
      (do (.close server)
          (on-close)
          (assoc this :server nil)))))

(defschema TCPServerOptions
  "Valid options to launch a tcp server"
  (->> [{:port (s/either (s/eq 0) Port)} {:socket-address SocketAddress}]
       (mapv #(assoc % (s/optional-key :ssl-context) SslContext))
       (apply s/either)))

(defschema Info
  "Information about a connection"
  {:remote-addr String
   :remote-port Port
   :server-name String})

(defschema Stream
  "A manifold stream"
  (s/pred stream? 'stream?))

(defschema Handler
  "A TCP Server callback"
  (s/=> s/Any Stream Info))

(def noop
  "A no op nullary"
  (fn []))

(defschema OnClose
  "A nullary to be run on server close"
  (s/=> s/Any))

(defnk tcp-server :- (s/protocol Lifecycle)
  [handler :- Handler, {on-close :- OnClose noop} options :- TCPServerOptions]
  (->TCPServer on-close handler options))

(defschema AcceptResult
  "The result of a accept try-put!"
  (s/either s/Bool (s/eq ::timeout)))

(defschema ResultHandler
  "Handles the result of accepting a connection"
  (s/=> s/Any Stream AcceptResult))

(s/defn failed? :- s/Bool
  "Determines if the put failed"
  [result :- AcceptResult]
  (or (false? result) (identical? ::timeout result)))

(s/defn forcibly-close
  "Closes the stream if the put failed"
  [s :- Stream, v :- AcceptResult]
  (if (failed? v)
    (close! s)
    (t/info "Accepted a connection")))

(defnk onto-sink :- Handler
  "Returns a handler that can be used to provide a stream api"
  [sink :- (s/pred sink? 'sink?) {handle-result :- ResultHandler forcibly-close}]
  (fn [s info]
    @(d/chain (try-put! sink (assoc info :stream s) 0 ::timeout)
             #(handle-result s %))))

(defnk close-stream
  "Returns a nullary function which closes a stream"
  [stream :- Stream]
  #(close! stream))
