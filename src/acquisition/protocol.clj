; TODO: document this namespace

(ns acquisition.protocol
  (:require [plumbing.core :refer :all]
            [schema.core :as s]
            [manifold.stream :as stream])
  (:import (java.util UUID Date)))

(s/defschema Sink
  (s/pred stream/sink?))

(def protocol
  (vector
    (let [route (s/fn [to connections]                      ; TODO: add schemas
                  (cond
                    (= :global to) (vals connections)
                    (instance? UUID to) [(get connections to)]
                    :else []))]
      (array-map :query (fnk [from]
                          (fnk [connections :- {UUID Sink}, protocol]
                            {:route/to (route from connections)
                             :from     :server
                             :version (keys protocol)
                             :time     (Date.)
                             :users    (keys connections)}))

                 :ask (fnk [from :- UUID, to]
                        (fnk [connections]
                          {:route/to (route to connections)
                           :action   :ask
                           :to       from}))

                 :message (fnk [from message to]
                            (fnk [connections]
                              {:route/to (route to connections)
                               :from     from
                               :message  message}))

                 :version! (fnk [from :- UUID, version :- s/Int]
                             (fnk [connections]
                               {:route/to (route from connections)
                                :state/swap! (fn [state] (assoc-in state [:info from :version] version))
                                :version version}))

                 :version (fnk [from]
                            (fnk [connections, info]
                              {:route/to (route from connections)
                               :version (get-in info [from :version])}))

                 :common (fnk [from :- UUID]
                           (fn [state]
                             (assoc-in state [:info from :last-seen] (Date.))))))))

(defnk build* [{common (constantly identity)} {default (fn [_ all] {:error :invalid-action :message all})} & handlers]
  (-> (into {}
            (map (fn [[k v]]
                   [k (s/schematize-fn (fn [state message]
                                         (update ((v message) state) :state/swap! #(if (nil? %) (common message) (comp % (common message)))))
                                       (s/=> s/Any s/Any s/Any) ; TODO: Accurate Schemas
                                       )]))
            handlers)
      (assoc :default default :dipsatch :action)))

(defnk build [dispatch :- s/Keyword default & m]
  (fn [state message]                                       ; TODO: add schema
    (let [action (get message dispatch)
          f (get m action default)]
      (f state message))))

(defn build-all
  [p]
  (mapv (comp build build*) p))