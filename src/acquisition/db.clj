(ns acquisition.db
  "A database component for acquisition
  also provides some async ops"
  (:require [com.stuartsierra.component :refer (Lifecycle)]
            [taoensso.timbre :as t]
            [schema.core :as s :refer (defschema)]
            [plumbing.core :refer (defnk)]
            [clojure.java.jdbc :as jdbc]
            [acquisition.pool :refer (on-pool)]
            [manifold.deferred :as d])
  (:import [java.util.concurrent Executor]))

;; schemas
(defschema Deferred
  "A deferred"
  (s/pred d/deferred? 'deferred?))

(defschema H2Spec
  "A h2 database spec for jdbc"
  {:classname (s/eq "org.h2.Driver")
   :subname s/Str
   :subprotocol (s/eq "h2")})

(defschema DBSpec
  "A jdbc database spec for acquisition"
  (s/either H2Spec))

(defschema Connection
  "A connection to a database"
  {:connection s/Any
   s/Any s/Any})

(defschema SQLVector
  "A query for JDBC"
  [(s/one String 'QueryTemplate) s/Any])

(defschema Row
  "A row in the database"
  {s/Keyword s/Any})

(defschema RowFn
  "A function to manipulate a row
  Cannot return a lazy seq"
  (s/=> s/Any Row))

(defschema SetFn
  "Manipulates the resulting set of rows"
  (s/=> s/Any [Row]))

(defschema QueryOptions
  "Options map to query"
  {:result-set-fn SetFn
   :row-fn RowFn})

(defschema Table
  "A database table"
  s/Keyword)

(defschema RowMap
  "A row to be inserted into a table"
  {s/Keyword s/Any})

(defrecord JDBCDatabase [spec connection]
  Lifecycle
  (start [this]
    (if connection this
      (assoc this :connection (jdbc/get-connection spec))))
  (stop [this]
    (if-not connection this
      (do (.close connection)
          (assoc this :connection nil)))))

(s/defn jdbc-database :- Connection
  [spec :- DBSpec]
  (->JDBCDatabase spec nil))

(s/defn ^:always-validate query ; always validate db interactions
  ([sql-query :- SQLVector]
   (query sql-query {:row-fn RowFn :result-set-fn SetFn}))
  ([sql-query :- SQLVector, opts :- QueryOptions]
   (s/fn ^:always-validate do-query
     ([db :- Connection]
      (jdbc/query db sql-query :result-set-fn (:result-set-fn opts) :row-fn (:row-fn opts)))
     ([db :- Connection, pool :- Executor]
      (on-pool pool (do-query db))))))

(s/defn ^:always-validate insert!
  [table :- Table, row-map :- RowMap & more-rows :- [RowMap]]
  (s/fn ^:always-validate do-insert!
    ([db :- Connection]
     (apply jdbc/insert! db table row-map `[~@more-rows :transaction? false]))
    ([db :- Connection, pool :- Executor]
     (on-pool pool (do-insert! db)))))

(s/defn ^:always-validate execute!
  [sql-vector :- SQLVector]
  (s/fn ^:always-validate do-execute!
    ([db :- Connection]
     (jdbc/execute! db sql-vector :transaction? false))
    ([db :- Connection, pool :- Executor]
     (on-pool pool (do-execute! db)))))
