(ns acquisition.backends
  (:require [plumbing.core :refer (defnk)]
            [manifold.stream :as stream]
            [schema.core :as schema])
  (:import (java.util UUID)))

(definterface IConnectionSource
  (connections []))

(defnk single-connection-source :- IConnectionSource
  [id :- UUID, stream :- (schema/pred stream/stream?)]
  (reify IConnectionSource
    (connections [_]
      (stream/->source [{:id id :stream stream :source #'single-connection-source}]))))