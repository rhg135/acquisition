(ns acquisition.udp
  "UDP Socket Component"
  (:require [com.stuartsierra.component :refer (Lifecycle)]
            [manifold.stream :as stream]
            [aleph.udp :as udp]
            [taoensso.timbre :as t]))

(defrecord UDPSocket [opts socket]
  Lifecycle
  (start [this]
    (if socket this
      (assoc this :socket @(udp/socket opts))))
  (stop [this]
    (if-not socket this
      (do (stream/close! socket)
          (assoc this :socket nil)))))

(defn- on-port [n] {:port n})

(defn udp-socket
  "returns a component which starts a aleph/udp/socket"
  [opts]
  (->UDPSocket opts nil))

(def ^{:arglists '([port])} udp-socket-on-port
  "Component factory which takes the port to bind"
  (comp udp-socket on-port))
