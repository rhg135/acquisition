(ns acquisition.polymorphic
  "Define multimethods using maps"
  (:require [plumbing.core :refer (defnk)]
            [schema.core :as s])
  (:import [clojure.lang MultiFn]))

(s/defschema Schema
  "Yo dawg, I heard you like schemas, so I made a schema about a schema"
  (s/protocol s/Schema))

(defn- build-schema ; TODO: handle multiple arities
  [input dispatch-schema returned]
  (let [f-schema (s/=> returned input)]
    {:dispatch (s/=> dispatch-schema input)
     :default f-schema
     :name s/Str
     (s/optional-key :hierarchy) s/Any
     :methods {dispatch-schema f-schema}}))

(defn- add-methods
  [mfn m]
  (doseq [[k v] m]
    (.addMethod mfn k v)))

(defn polymorphic
  "Returns a polymorphic function"
  [options]
  (doto (MultiFn. (:name options) (:dispatch options) ::default (or (:hierarchy options) #'clojure.core/global-hierarchy)) ; TODO: that's private
    (add-methods (:methods options))
    (.addMethod ::default (:default options))))

(s/defn polymorphic-builder
  [input :- Schema, output :- Schema, dispatch :- Schema]
  (s/schematize-fn polymorphic (s/=> output (build-schema input dispatch output))))
