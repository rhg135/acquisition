(ns acquisition.state
  "State Management for acquisition"
  {:author "Ricardo H. Gomez"}
  (:require [schema.core :as s :refer (defschema)]
            [acquisition.db :as db]
            [manifold.stream :as stream])
  (:import [java.util UUID]))

(defschema Sink
  "A manifold sink, where you can put!"
  (s/pred stream/sink? 'sink?))

(defschema Nick
  "A nickname on acquisition akin to a twitter handle"
  s/Symbol)

(def Channel
  "A channel on acquisition akin to an IRC channel"
  s/Str)

(defschema Tag
  "A tag on acquisition akin to a hashtag I think"
  s/Keyword)

(defschema Connection
  "Data about a connection"
  {:sink Sink
   :nicks [Nick]
   :channels [Channel]
   :tags [Tag]})

(defschema Connections
  "Stores information on all the (volatile) connections"
  {UUID Connection})

(defn- new-nick
  []
  (gensym "nick-"))

(s/defn add-connection :- Connections
  "Adds a new connection to the connections map"
  [connections :- Connections, id :- UUID, sink :- Sink]
  (assoc connections id {:sink sink :nicks #{(new-nick)} :channels #{} :tags #{}}))

(s/defn del-connection :- Connections
  "Removes a connection from the connections map"
  [connections :- Connections, id :- UUID]
  (dissoc connections id))

(s/defn add-nick :- Connections
  "Returns a connections map where connection id has `nick` as a nick"
  [connections :- Connections, id :- UUID, nick :- Nick]
  (update-in connections [id :nicks] conj nick))

(s/defn del-nick :- Connections
  "Returns a connections map where connection id `id` doesn't have `nick` as a nick"
  [connections :- Connections, id :- UUID, nick :- Nick]
  (update-in connections [id :nicks] disj nick))

(defn- db-join [n c]
  (db/insert! :channels {:nick n :channel c}))

(defn- find-nick
  [cxs n]
  (for [[id conn] cxs
        :when (contains? (:nicks conn) n)]
    id))

(defn- join-all
  [cxs c ids]
  (reduce #(update-in %1 [%2 :channels] conj c)
          cxs
          ids))

(defn- mem-join [connections n c]
  (->> (find-nick connections n)
       (join-all connections c)))

(s/defn join-channel
  "Returns a function which joins `nick` to `channel`
  if volatile-nick? then no db access required
  otherwise returns a function as per db/insert!"
  [nick :- Nick, channel :- Channel, volatile-nick? :- s/Bool]
  (if-not volatile-nick?
    (db-join nick channel)
    (s/fn [connections :- Connections]
      (mem-join connections nick channel))))

(defn- part-all
  [cxs c ids]
  (reduce #(update-in %1 [%2 :channels] disj c)
          cxs
          ids))

(defn- mem-part [n c]
  (s/fn [cxs :- Connections]
    (->> (find-nick cxs n)
         (part-all cxs c))))

(defn- db-part [n c]
  (db/execute! ["DELETE FROM channels WHERE channel = ?" (name c)])) ; TODO: this feels hackish

(s/defn part-channel
  [nick :- Nick, channel :- Channel, volatile-nick? :- s/Bool]
  (if volatile-nick?
    (mem-part nick channel)
    (db-part nick channel)))

(defn- add-tag-all
  [cxs c ids]
  (reduce #(update-in %1 [%2 :tags] conj c)
          cxs
          ids))

(defn- mem-add-tag [n t]
  (s/fn [cxs :- Connections]
    (->> (find-nick cxs n)
         (add-tag-all cxs t))))

(defn db-add-tag [n t]
  (db/insert! :tags {:nick n :tag t}))

(s/defn add-tag
  [nick :- Nick, tag :- Tag, memory? :- s/Bool]
  (if memory?
    (mem-add-tag nick tag)
    (db-add-tag nick tag)))

(defn- del-tag-all
  [cxs c ids]
  (reduce #(update-in %1 [%2 :tags] disj c)
          cxs
          ids))

(defn- mem-del-tag [n t]
  (s/fn [cxs :- Connections]
    (->> (find-nick cxs n)
         (del-tag-all cxs t))))

(defn db-del-tag [n t]
  (db/execute! ["DELETE FROM tags WHERE tag = ?" (name t)])) ; eww, mutability

(s/defn del-tag
  [nick :- Nick, tag :- Tag, memory? :- s/Bool]
  (if memory?
    (mem-del-tag nick tag)
    (db-del-tag nick tag)))
