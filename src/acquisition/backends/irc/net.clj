(ns acquisition.backends.irc.net
  "Network bits of the IRC support"
  (:require [clojure.string :as string]
            [gloss.io :as gio]
            [gloss.core :as gloss]
            [manifold.stream :as s]
            [schema.core :as schema]
            [plumbing.core :refer (defnk fnk)]
            [acquisition.from.irclj.parser :as parser]))

(schema/defn params-to-string :- String
  [params :- [String]]
  (str (string/join \space (butlast params)) \: (last params))) ; Two traversals; meh O(2n) vs O(n)

(defnk ->irc :- String
  [command :- String
   params :- [String]
   host :- (schema/maybe String)
   nick :- String
   user :- (schema/maybe String)]
  (str ":" nick (if (and user host) (str "!" user "@" host)) " " command " " (params-to-string params)))

(def irc-frame
  (gloss/string :utf-8 :delimiters ["\r\n" "\n"]))

(defn wrap-duplex-stream
  [protocol s]
  (let [out (s/stream)]
    (s/connect
      (s/map #(gio/encode protocol %) out)
      s)
    (s/splice
      out
      (gio/decode-stream s protocol))))

(def irc-protocol
  (gloss/compile-frame irc-frame
                       ->irc
                       parser/parse))