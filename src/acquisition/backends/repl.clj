(ns acquisition.backends.repl
  (:require [plumbing.core :refer :all]
            [manifold.stream :as stream]
            [manifold.deferred :as d]
            [acquisition.backends :as backends]
            [schema.core :as schema]
            [plumbing.fnk.pfnk :as pfnk])
  (:import (java.util UUID)
           (acquisition.backends IConnectionSource)))

(defnk new-handle
  "Returns a map with an :id and :stream key
  additionally provides :sink and :source keys for repl use
  :id can be provided to use it instead of the random UUID"
  [{id :- UUID (UUID/randomUUID)}]
  (let [source (stream/stream)
        sink (stream/stream)]
    {:id     id
     :stream (stream/splice sink source)
     :sink sink
     :source source}))

(def repl-backend
  "Returns a pair of a map returned by `new-handle` and an `IConnectionSource` of it"
  (comp (juxt identity backends/single-connection-source) new-handle))

(alter-var-root #'repl-backend schema/schematize-fn
                (schema/=> [(schema/one (pfnk/output-schema new-handle) 'Connection) (schema/one IConnectionSource IConnectionSource)]
                           (pfnk/input-schema new-handle)))