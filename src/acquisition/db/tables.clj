(ns acquisition.db.tables
  "The tables we use"
  {:author "Ricardo H. Gomez"}
  (:gen-class)
  (:require [acquisition.db :as db]
            [clojure.java.jdbc :as jdbc]
            [schema.core :as s]))

(def create-nick-table
  (db/execute! ["CREATE TABLE nick(nick VARCHAR_IGNORECASE PRIMARY KEY, real_name VARCHAR_IGNORECASE)"]))

(def create-channel-table
  (db/execute! ["CREATE TABLE channel(channel VARCHAR_IGNORECASE PRIMARY KEY)"]))

(def create-tag-table
  (db/execute! ["CREATE TABLE tag(tag VARCHAR_IGNORECASE PRIMARY KEY)"]))

(def create-tags-table
  (db/execute! ["CREATE TABLE tags(tag VARCHAR_IGNORECASE NOT NULL, nick VARCHAR_IGNORECASE NOT NULL)"]))

(def create-channels-table
  (db/execute! ["CREATE TABLE channels(channel VARCHAR_IGNORECASE NOT NULL, nick VARCHAR_IGNORECASE NOT NULL)"]))

(defn h2-database [file-name]
  {:classname "org.h2.Driver"
   :subname file-name
   :subprotocol "h2"})

(defn -main
  [file-name]
  (s/validate String file-name)
  (let [db (h2-database file-name)]
    (jdbc/db-transaction* db (fn [conn]
                               (create-nick-table conn)
                               (create-channel-table conn)
                               (create-tag-table conn)
                               (create-tags-table conn)
                               (create-channels-table conn)))))
