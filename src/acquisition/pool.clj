(ns acquisition.pool
  "Thread pool component for leaven"
  (:require [com.stuartsierra.component :refer (Lifecycle)]
            [schema.core :as s]
            [plumbing.core :refer (defnk)]
            [manifold.deferred :as d]
            [aleph.flow :refer (utilization-executor)])
  (:import [java.util.concurrent Executor Executors ExecutorService]))

(defn- wrap [f]
  (let [d (d/deferred)]
    (vector d #(try
                (d/success! d (f))
                (catch Exception e
                  (d/error! d e))))))

(defn on-pool* [pool f]
  (let [[d f] (wrap f)]
    (.execute pool f)
    d))

(defmacro on-pool [pool & body]
  "Runs the body of code in a Executor and returns a deferred"
  `(on-pool* ~pool (fn [] ~@body)))

(defrecord UtilizationPool [target-utilization max-threads opts pool]
  java.util.concurrent.Executor
  (execute [_ r]
    (.execute pool r))
  Lifecycle
  (start [this]
    (if pool this
             (assoc this :pool (utilization-executor target-utilization max-threads opts))))
  (stop [this]
    (if (not pool) this
                   (do (.shutdownNow ^io.aleph.dirigiste.Executor pool)
                       (assoc this :pool nil)))))

(s/defschema Utilization
  "The ratio of threads to be busy"
  (s/pred #(<= 0 % 1) 'decimal-ratio?))

(defnk utilization-pool :- (s/protocol Lifecycle)
  [{multiplier :- s/Num 8} {target-utilization :- Utilization 0.9} & opts]
  (-> (.availableProcessors (Runtime/getRuntime))
      (* multiplier)
      int
      (as-> $ (->UtilizationPool target-utilization $ opts))))
