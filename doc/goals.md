# Goals

## Experience

Imagine twitter + irc + hangouts.

A hashtag is a group of nicks (described later) which receive any messages mentioning the channel name.
This is similar to how twitter handles a "conversation".

A channels is a group of nicks (described later), which receives any messages addressed to it.
Similar to IRC channels.

A nick is a set of connections, which can receive messages.
Similar to hangouts' persistent connections or an IRC bouncer.

Notice there is no equivalent to IRC nicks--well there is connections, but that's an implementation detail.

## Objectives

  * Dynamic
    * Open sourced
    * Versioned
  * Efficient
    * Battery
      * No constant connection
      * Be available even when you aren't
    * Bandwidth
      * Different transmission formats
  * Useful
    * Open source
    * Featured
  * Compatible
    * IRC backend
