# Formats

This file aims to describe many data formats used in acquisition.

## Message

This is map with a :action key. The values thereof and their implications will be
discussed below.

### :query

*No further required keys*
Petitions the server to reply with some statistic described below.

### :ask

  * :to - the user you want to ask for information.

Asks an user for statistics, the statistics are client dependent.

### :message

  * :to - the user which to send to or :global
  * :message - the message to send as a string.

Sends a message to an user or all users.

### :version!

  * :version - an integer of a valid version to set

Sets it to the new version and replies with the new version

### :version

replies with the current version as :version!

## Statistics

### Server

A map with:
  * :date - the current date (java.util.Date in this implementation)
  * :users - a sequence of all the connections in the state.
  * :versions - a sequence of the protocol versions this server has