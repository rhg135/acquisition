(defproject acquisition "0.0.1-SNAPSHOT"
  :description "IRC for the modern world"
  :url "https://bitbucket.org/rhg135/acquisition"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :aliases {"init-tables" ["run" "-m" "acquisition.db.tables"]
            "gen-doc" ["with-profile" "doc" "marg" "-d" "doc" "-f" "literate.html"]}
  :profiles {:doc {:dependencies [[org.clojure/clojure "1.6.0"]]
                   :plugins [[lein-marginalia "0.8.0"]]}
             :dev {:shorthand {. [taoensso.timbre/spy
                                  com.stuartsierra.component/start
                                  com.stuartsierra.component/stop]}
                   :dependencies [[org.clojure/tools.namespace "0.2.11"]]
                   :source-paths ["dev"]}}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [com.h2database/h2 "1.4.187"]
                 [org.clojure/java.jdbc "0.3.7"]
                 [com.taoensso/timbre "4.0.2"]
                 [aleph "0.4.0"]
                 [manifold "0.1.0"]
                 [gloss "0.2.5"]
                 [com.stuartsierra/component "0.2.3"]
                 [prismatic/plumbing "0.4.4"]
                 [prismatic/schema "0.4.3"]])
