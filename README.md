# acquisition

IRC for the modern world.

## Status

Pre-development

## Usage

None yet. Just another fruitless idea.

## Docs

In [here](https://bitbucket.org/rhg135/acquisition/doc).

## License

Copyright © 2015 Ricardo Gomez

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
