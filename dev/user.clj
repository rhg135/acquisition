(ns user
  (:use clojure.repl
        clojure.reflect)
  (:require [clojure.tools.namespace.repl :refer (refresh disable-reload!)]
            [com.stuartsierra.component :as c]))

(def system nil)

(defn system! [val]
  (alter-var-root #'system (constantly val)))

(defn start []
  (alter-var-root #'system c/start))

(defn stop []
  (alter-var-root #'system c/stop))

(defn go []
  (alter-var-root (comp c/start c/stop)))

(disable-reload!)

(defn refresh! []
  (stop)
  (refresh))
